# mini fantasy

Voici le mini-projet Fantasy de Berault Xavier et Bellot Tristan.

### Avancée:
Un joueur peut créer un monde de taille voulue.<br/> 
Il peut choisir le nombre D'elfes et le nombre d'elfes qu'il souhaite controler.<br/>
Il peut choisir le nombre de Gnomes.<br/>
Les Elfes non joueurs ne font que se déplacer à gauche.<br/>
Les joueurs incarnent les Elfes et à chaque tour peuvent choisir de se déplacer, attendre ou creer une tribu. <br/>
Une fois une tribu créée, les elfes débloquent la possibilité de soliciter les Gnomes sur les parcelles adjascentes pour les ajouter à leur tribu, de negocier avec les elfes des tribus adjascentes pour assimiler leur tribu ou de s'émanciper et quitter leur tribu. <br/>
Les Gnomes fuient les Elfes de tribus ennemies et se réfugient vers leur chef de tribu.<br/>

Il n'y a pas de condition de victoire et pour arreter le jeu il faut taper "!" au début d'un nouveau tour.

L'image diagrammeInitial.png montre le diagramme de notre projet que nous avions en tête avant de coder.<br/>
L'image diagrammeFinal.png montre l'organisation de projet à la fin du développement.

### Compilation:
Pour compiler, placez vous dans le dossier mini-fantasy et tapez la commande : 
javac -d .out src/*.java src/Events/*.java src/Utils/*.java

### Execution
Après la compialtion, rendez vous dans le dossier .out et executez la commande :
java Fantasy