import Events.IEvent;

public class Recrutement implements IEvent {
    /**
     * Personne qui veut recruter
     */
    private Race recruteur;

    /**
     * Personne essayé d'être recruté
     */
    private Race recrute;

    public Recrutement(Race recruteur, Race recrute) {
        this.recruteur = recruteur;
        this.recrute = recrute;
    }

    @Override
    public void consume() {
        this.recrute.rejoindreTribu(this.recruteur.getTribu());
    }
}
