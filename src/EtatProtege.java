import java.util.Set;

/**
 *
 */
public class EtatProtege implements IEtat {

    private RaceEtat individu;

    /**
     * Default constructor
     */
    public EtatProtege(RaceEtat individu) {
        this.individu = individu;
    }

    /**
     *
     */
    public void reaction() {

    }

    @Override
    public boolean isTrigerred() {
        Set<Race> ensHabitants = this.individu.getParcelle().getEnsHabitant();
        Tribu tribu = this.individu.getTribu();
        for (Race mb : ensHabitants)
            if (mb instanceof Elfe && mb.getTribu().equals(tribu) && mb.equals(tribu.getChef()))
                return true;
        return false;
    }


    public RaceEtat getIndividu() {
        return individu;
    }

    public void setIndividu(RaceEtat individu) {
        this.individu = individu;
    }
}