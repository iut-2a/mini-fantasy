import Utils.Color;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
public class Parcelle {
    private static int IDPARCELLE;
    /**
     * permet de rendre toutes les parcelles différentes pour tester les égalités
     */
    private int id;
    /**
     * La tuile actuelle correspondante à la parcelle
     */
    private String tile;
    /**
     * Un ensemble contenant tous les individus présents sur cette parcelle
     */
    private Set<Race> raceSet;


    /**
     * Default constructor
     */
    public Parcelle() {
        this.id = ++Parcelle.IDPARCELLE;
        this.raceSet = new HashSet<>();
    }

    /**
     * Permet d'avoir un affichage textuel d'une parcelle
     */
    @Override
    public String toString() {
        Map<String, Integer> dict = new HashMap<>();
        String res = new String("[ ");
        for (Race individu : this.raceSet) {
            if (dict.containsKey(individu.getPrintValue())) {
                dict.put(individu.getPrintValue(), dict.get(individu.getPrintValue()) + 1);
            } else {
                dict.put(individu.getPrintValue(), 1);
            }
        }
        for (String race : dict.keySet()) {
            res += dict.get(race);
            res += race;
            res += " ";
        }
        return res + ']';

    }

    public Set<Race> getEnsHabitant() {
        return this.raceSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parcelle parcelle = (Parcelle) o;
        return id == parcelle.id && Objects.equals(tile, parcelle.tile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tile);
    }

    public void ajouterRace(Race toAdd) {
        this.raceSet.add(toAdd);
    }

    public void retirerRace(Race toRemove) {
        this.raceSet.remove(toRemove);
    }

    /**
     * retourne tous les individus de la parcelle étant de la race voulue
     *
     * @param cToFind la classe des individus à chercher
     * @return un ensemble de race correspondant à celle demandée
     */
    public Set<Race> getMatching(Class<? extends Race> cToFind) {
        Set<Race> res = new HashSet<>();
        res.addAll(this.getEnsHabitant().stream().filter(cToFind::isInstance).collect(Collectors.toList()));
        return res;
    }

    public String toStringWithRace(Race ind) {
        Map<String, Integer> dict = new HashMap<>();
        String res = new String("[ ");
        for (Race individu : this.raceSet) {
            if (individu.equals(ind)) {
                if (dict.containsKey(individu.getPrintValue())) {
                    dict.put(Color.GREEN_BACKGROUND_BRIGHT + individu.getPrintValue(), dict.get(individu.getPrintValue()) + 1);
                } else {
                    dict.put(Color.GREEN_BACKGROUND_BRIGHT + individu.getPrintValue(), 1);
                }
            } else {
                if (dict.containsKey(individu.getPrintValue())) {
                    dict.put(individu.getPrintValue(), dict.get(individu.getPrintValue()) + 1);
                } else {
                    dict.put(individu.getPrintValue(), 1);
                }
            }

        }
        for (String race : dict.keySet()) {
            res += dict.get(race);
            res += race;
            res += " ";
        }
        return res + ']';
    }
}