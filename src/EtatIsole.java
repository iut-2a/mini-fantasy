import java.util.Objects;
import java.util.Set;

/**
 * Un individu se sent isolé s'il n'y a pas d'elfe sur sa position
 */

public class EtatIsole implements IEtat {

    private RaceEtat individu;

    /**
     * Default constructor
     */
    public EtatIsole(RaceEtat individu) {
        this.individu = individu;
    }

    /**
     *
     */
    public void reaction() {
        individu.refugier();
    }

    /**
     * Un individu se sent isolé lorsqu'aucun elfe de sa tribu se situe sur sa parcelle
     *
     * @return un boléen qui indique si l'individu est isolé
     */
    @Override
    public boolean isTrigerred() {
        Set<Race> ensHabitants = this.individu.getParcelle().getEnsHabitant();
        Tribu tribu = this.individu.getTribu();
        for (Race mb : ensHabitants)
            if (mb instanceof Elfe && mb.getTribu().equals(tribu))
                return true;
        return false;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EtatIsole etatIsole = (EtatIsole) o;
        return Objects.equals(individu, etatIsole.individu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(individu);
    }
}