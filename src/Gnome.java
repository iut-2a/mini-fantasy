import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 *
 */
public class Gnome extends RaceEtat {
    /**
     * Contient tous les états communs aux gnomes
     */
    @SuppressWarnings("unchecked")
    private static Class<? extends IEtat>[] ETATSGNOME = new Class[]{EtatIsole.class, EtatProtege.class, EtatVulnerable.class};

    /**
     * Default constructor
     */
    public Gnome(Monde monde) {
        super(monde);
        this.initialiserEtats();
    }

    /**
     * initialise les états par défauts du gnome
     */
    @SuppressWarnings("unchecked")
    private void initialiserEtats() {
        for (Class<? extends IEtat> cEtat : Gnome.ETATSGNOME) {
            try {
                super.ajouterEtat(cEtat.getConstructor(RaceEtat.class).newInstance(this));
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        super.changeEtat.changerEtat();
    }


    // le Gnome essaye d'aller vers son chef de tribu
    @Override        // TODO implement here

    public void refugier() {
        if (Objects.nonNull((this.getTribu())) && Objects.nonNull(this.getTribu().getChef())) {
            System.out.println("je vais vers mon chef");
            List<Integer> posChef = this.monde.getPos(this.getTribu().getChef().getParcelle());
            List<Integer> posPerso = this.monde.getPos(this.parcelle);

            int deltaX = posChef.get(0) - posPerso.get(0);
            int deltaY = posChef.get(1) - posPerso.get(1);

            if (deltaX!=0 ||deltaY!=0){
                if (Math.abs(deltaX) > Math.abs(deltaY)) { // on regarde quelle distance est la plus grande afin de s'approcher du chef
                    if (deltaX > 0) { //si le delta est supérieur à 0 alors le gnome est à l'ouest du chef, il doit donc aller à l'est
                        this.seDeplacer('e');
                    } else {
                        this.seDeplacer('o');
                    }
                } else {
                    if (deltaY > 0) {
                        this.seDeplacer('s');
                    } else {
                        this.seDeplacer('n');
                    }
                }
            }
        }
        else{
            Random rd = new Random();
            float a = rd.nextFloat();
            if (a < 0.25){
                this.seDeplacer('n');
            }
            else if (a <0.5){
                this.seDeplacer('e');
            }
            else if (a <0.75){
                this.seDeplacer('s');
            }
            else{
                this.seDeplacer('o');
            }
        }
        }

    @Override
    public void fuir(Race chefEtranger) {
        List<Integer> posChef = this.monde.getPos(chefEtranger.getParcelle());
        List<Integer> posPerso = this.monde.getPos(this.parcelle);

        int deltaX = posChef.get(0) - posPerso.get(0);
        int deltaY = posChef.get(1) - posPerso.get(1);

        if (Math.abs(deltaX) < Math.abs(deltaY)) { // on regarde quelle distance est la plus petite pour fuir le chef de la tribu étrangère
            if (deltaX < 0) { //si le delta est supérieur à 0 alors le gnome est à l'est du chef, il doit donc aller encore plus à l'est pour fuir
                this.seDeplacer('e');
            } else {
                this.seDeplacer('o');
            }
        } else {
            if (deltaY < 0) {
                this.seDeplacer('s');
            } else {
                this.seDeplacer('n');
            }
        }


    }

    /**
     * @param tribu fiat quitter sa tribu à un Gnome et le fait rejoindre une autre
     */
    @Override
    public void deserter(Tribu tribu) {
        this.quitterTribu();
        this.rejoindreTribu(tribu);
    }

    @Override
    public void recruter(Race individu) {
    }

    @Override
    public void reagir() {
        super.changeEtat.deplacement();
    }

}