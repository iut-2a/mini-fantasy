import java.util.Objects;

/**
 *
 */
public abstract class Membre {
    /**
     *
     */
    private Tribu tribu;

    /**
     * Default constructor
     */
    public Membre() {
    }

    /**
     * créer un membre à partir d'une tribu
     *
     * @param t la tribu à laquelle appartiendra le nouveau membre
     * @return le nouveau membre
     */
    public static Membre membreFromTribu(Tribu t) {
        Membre mb = new Tribu();
        mb.rejoindreTribu(t);
        return mb;
    }

    /**
     * Lorsqu'il rejoint une tribu, il quitte sa tribu actuelle
     *
     * @param t
     */
    public void rejoindreTribu(Tribu t) {
        this.tribu = t;
    }

    /**
     * Quitte la tribu actuelle du membre ( null )
     * S'il s'agit du chef d'une tribu, celle-ci n'en aura plus
     */
    public void quitterTribu() {
        if (Objects.isNull(this.tribu) || Objects.isNull(this.tribu.getChef()))
            return;
        if (this.tribu.getChef().equals(this))
            this.tribu.mettreChef(null);
        this.tribu = null;
    }

    /**
     * @return un boléen qui indique si le membre possède actuellement une tribu
     */
    public boolean possedeTribu() {
        return Objects.nonNull(this.tribu);
    }

    /**
     * @return la tribu auquel appartient le membre
     */
    public Tribu getTribu() {
        return this.tribu;
    }

}