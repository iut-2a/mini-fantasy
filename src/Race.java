import Events.IEvent;
import Utils.Color;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 *
 */
public abstract class Race extends Membre {
    private static int RACEID = 0;
    /**
     * la parcelle sur laquelle se trouve cet individu
     */
    protected Parcelle parcelle;
    /**
     * Le monde dans lequel cet individu va évoluer
     */
    protected Monde monde;
    private int id;

    /**
     * Liste des interactions effectuées sur l'individu
     * vidée à chaque tour de l'individu
     * Si un habitant se donne un évènement ç lui même, celui-ci ne sera pas consumé
     */
    private List<IEvent> interactionList;

    /**
     * Boléen qui indique si le joueur est une ia ou un humain
     * true pour un humain et false une ia
     */
    private boolean isPlayed;


    /**
     * Default constructor
     */
    public Race(Monde monde) {
        super();
        this.id = ++Race.RACEID;
        this.monde = monde;
        this.isPlayed = false;
        this.parcelle = this.monde.getParcelle(0, 0);
        this.interactionList = new ArrayList<>();
    }

    public Race(Monde monde, int posX, int posY) {
        super();
        this.parcelle = this.monde.getParcelle(posX, posY);
    }

    /**
     * @param individu
     */
    public abstract void recruter(Race individu);

    /**
     * Par défaut il jouera son tour
     */
    public void reagir() {
        for (IEvent event : this.interactionList)
            event.consume();
        LibAction.effectuerAction(this);
    }

    /**
     * @param direction permet à un individu de se déplacer d'une case dans une direction
     */
    public void seDeplacer(char direction) {
        List<Integer> pos = this.monde.getPos(this.parcelle);
        int x = pos.get(0);
        int y = pos.get(1);
        switch (direction) {
            case ('s'):
                ++y;
                break;
            case ('n'):
                --y;
                break;
            case ('e'):
                ++x;
                break;
            case ('o'):
                --x;
                break;
            default:
                System.out.println("direction non comprises " + direction);
        }
        System.out.println("déplacement vers " + x + " " + y);
        this.parcelle = this.monde.deplacer(this, x, y);
    }

    public Parcelle getParcelle() {
        return this.parcelle;
    }

    public void setParcelle(Parcelle toSet) {
        this.parcelle = toSet;
    }

    /**
     * permet de représenter un individu en fonction de sa race
     * par défaut ça reverra *
     *
     * @return un string permettant d'identifier la race en question
     * ce dernier possèdera les couleurs correspantes à la tribu de l'individu
     */
    public String getPrintValue() {
        String res = "";
        if (Objects.nonNull(this.getTribu()))
            res += this.getTribu().getColor();
        return res + "*" + Color.RESET;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Race race = (Race) o;
        return id == race.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void addEvent(IEvent event) {
        this.interactionList.add(event);
    }

    public List<IEvent> getEvents() {
        return this.interactionList;
    }

    /**
     * supprime tous les events dont l'individu a été la cible
     */
    public void videEvent() {
        this.interactionList.clear();
    }

    public boolean isPlayed() {
        return this.isPlayed;
    }

    /**
     * définit qui jouera cet individu
     *
     * @param estJouee un boléen à true sur le joueur est humain
     */
    public void setPlayed(boolean estJouee) {
        this.isPlayed = estJouee;
    }


    public Monde getMonde() {
        return this.monde;
    }

    /**
     * Exécute l'action passée en paramètre
     *
     * @param action l'action à effectuer
     */
    public void effectuer(Action action) {
        action.execute(this);
    }

    public final char getDirectionDeplacement() {
        if (this.isPlayed())
            return LibAction.askDirectionDeplacement();
        else
            return this.getDirectionDeplacementIA();

    }

    /**
     * retourne toutes les actions disponibles pour l'individu en questiuon
     * par défaut, il ne pourra que passer son tour
     *
     * @return un tableau d'action
     */
    public Action[] getActionDisponibles() {
        return new Action[]{Action.Attendre};
    }

    /**
     * @return un str contenant toutes les actions réalisables par l'individu
     */
    public final String getActionsJoueur() {
        String res = "Action disponibles :\n";
        for (Action action : this.getActionDisponibles())
            res += action.toString() + " ";
        return res;
    }


    protected char getDirectionDeplacementIA() {
        return 'e';
    }


    public List<Action> getActionList() {
        return new ArrayList<>();
    }

    /**
     * @return l'id de l'elfe
     */
    public int getId() {
        return this.id;
    }

    /**
     * le comparateur de race fonctionne de la manière suivante:
     * - on fait les plus grande tribus en premières
     * - ensuite on met les chefs de cette tribu en premiers et les membres après
     *
     * @return le comparateur en question
     */
    public static Comparator<Race> getRaceComparator() {
        return new Comparator<Race>() {
            @Override
            public int compare(Race o1, Race o2) {
                if (Objects.isNull(o1.getTribu()) || Objects.isNull(o2.getTribu()))
                    return 0;
                if (o1.getTribu().getHabitants().size() > o2.getTribu().getHabitants().size()) {
                    if (o1.getTribu().getChef().equals(o1)) {
                        return 2;
                    }
                    return 1;
                }
                return 0;
            }
        };
    }
}