import Events.IEvent;
import Utils.Color;

import java.util.*;

/**
 * Classe qui gère la création du monde de fantasy et l'interactions personnages/monde
 * Contient l'exécutable
 */
public class Fantasy {
    /**
     * Monde actuel de fantasy
     */
    private Monde md;

    /**
     * liste de tous les habitants du monde
     */
    private List<Race> ensHabitants;

    /**
     * Numéro du tour/jour actuel dans le monde
     */
    private int numeroTour;

    public Fantasy() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Quelle taille désirez-vous pour le plateau ( couple x y ) \nOn vous conseille une taille de 9 9 ");
        int x = sc.nextInt();
        int y = sc.nextInt();
        this.md = new Monde(x, y);
        this.ensHabitants = new ArrayList<>();
        this.numeroTour = 0;
    }

    public static void main(String[] args) {
        Fantasy f = new Fantasy();
        f.creerEtPlacerHabitants();
        f.mainLoop();
    }

    /**
     * créer et ajoute des habitant au monde
     */
    private void creerEtPlacerHabitants() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Combien d'elfes voulez-vous dans le jeu ? ");
        int nbElfes = sc.nextInt();
        System.out.println("Combien d'elfes voulez-vous contrôler ? ");
        int nbElfesJoues = sc.nextInt();
        for (int i = 0; i < nbElfes; ++i) {
            Elfe hab = new Elfe(this.md);
            this.ensHabitants.add(hab);
            if (i < nbElfesJoues)
                hab.setPlayed(true);
        }
        System.out.println("Combien de gnomes voulez-vous dans le jeu ? ");
        int nbGnomes = sc.nextInt();
        for (int i = 0; i < nbGnomes; ++i)
            this.ensHabitants.add(new Gnome(this.md));
        Random rd = new Random();
        int i = 0;
        int j = 0;
        for (Race habitant : this.ensHabitants) {
            Parcelle nv = this.md.deplacer(habitant, rd.nextInt(this.md.getTailleX()), rd.nextInt(this.md.getTailleY()));
            habitant.setParcelle(nv);
        }
    }

    private void mainLoop() {
        Scanner sc = new Scanner(System.in);
        System.out.println("debut presentation du monde, entrez ! à n'importe quel moment pour arrêter");
        System.out.println(this.md);
        while (!sc.nextLine().equals("!")) {
            this.procEvents();
            System.out.println(this.md);
        }
    }

    /**
     * Génère et réalise toutes les actions des habitants du monde
     */
    private void procEvents() {
        System.out.println(Color.BLUE_BOLD + "Début du jour numéro " + ++this.numeroTour + Color.RESET);
        this.ensHabitants.sort(Race.getRaceComparator()); // on trie à chaque tour d'après nos paramètres
        for (Race habitant : this.ensHabitants) {
            if (habitant.isPlayed())
                System.out.println(this.md.toStringTour(habitant));
            System.out.println("----------------------------------------");
            String race = habitant.getClass().toString();
            race = race.substring(6, race.length()); // permet d'enlever class Race du str
            System.out.println("Au tour de " + race + " numéro " + habitant.getId());
            habitant.reagir();
            habitant.videEvent();
            System.out.println("----------------------------------------\n");
        }
        System.out.println(Color.RED_BOLD +
                "\n----------------------------------------");
        System.out.println("Fin du jour numéro " + this.numeroTour);
        System.out.println("----------------------------------------" + Color.RESET);
    }
}
