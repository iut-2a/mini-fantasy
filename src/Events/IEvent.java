package Events;

/**
 *
 */
public interface IEvent {

    /**
     * Consumme l'évènement et déclenche son effet
     */
    public void consume();

}