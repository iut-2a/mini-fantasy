import java.lang.reflect.Method;
import java.util.Scanner;

/**
 * Fournit les fonctions pour des exécutions simplifiées des déplacements des individus
 */
public class LibAction {

    /**
     * effectue l'action qui sera utilisée ce tour par l'individu
     */
    public static void effectuerAction(Race individuControle) {
        Method res;
        if (individuControle.isPlayed())
            LibAction.actionHumain(individuControle);
        else
            LibAction.actionIA(individuControle);
    }

    private static void actionHumain(Race individuControle) {
        System.out.println(individuControle.getActionsJoueur());
        Scanner sc = new Scanner(System.in);
        while (!executeFromInput(individuControle, sc.nextLine()))
            System.out.println("Votre commande n'a pas été comprise par le système");
        return;
    }

    /**
     * exécute l'action demandée par l'utilisateur
     * si l'utilisateur en demande une dont il ne possède pas les droits, celle-ci sera refusée
     *
     * @param action un string qui correspond à ce que l'on a demandé de faire
     * @return boléen qui indique si l'action s'est exécutée correctement
     */
    private static boolean executeFromInput(Race individuControle, String action) {
        try {
            action = action.substring(0, 1).toUpperCase() + action.substring(1); // majuscule permière lettre et minuscule après
            Action toDo = Action.valueOf(action);
            if (individuControle.isPlayed() && !individuControle.getActionList().contains(toDo))
                return false;
            toDo.execute(individuControle);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    final static char askDirectionDeplacement() {
        System.out.println("choisissez la direction dans laquelle vous souhaitez vous diriger ( o e s n )");
        Scanner sc = new Scanner(System.in);
        char res = 'a';
        boolean dejaVu = false;
        while (res != 'o' && res != 'e' && res != 's' && res != 'n') {
            if (dejaVu)
                System.out.println("Votre direction n'a pas été comprise, veuillez vous diriger vers l'une de ces directions : o e s n");
            res = sc.nextLine().charAt(0);
            dejaVu = true;
        }
        return res;
    }

    /**
     * exécute l'action de l'IA pour le tour actuel
     * par défaut l'IA va vouloir se déplacer
     */
    private static void actionIA(Race individuControle) {
        Action.Deplacer.execute(individuControle);
    }
}

