import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class AutomateEtat {

    /**
     * Etat actuel de l'individu contrôlé
     * Ne peut en avoir qu'un seul
     */
    private IEtat etatActuel;
    /**
     * individu dont on va influencer ses actions
     */
    private RaceEtat individuControle;

    /**
     * Liste qui contient tous les états possédés par l'individu
     */
    private List<IEtat> etatList;

    /**
     * Default constructor
     */
    public AutomateEtat(RaceEtat individu) {
        this.individuControle = individu;
        this.etatList = new ArrayList<>();
    }

    /**
     * Ajoute un état à gérer pour l'automate
     * Il faut les ajouter dans l'ordre de priorité de l'individu
     *
     * @param etat l'etat à ajouter
     */
    public void ajouterEtat(IEtat etat) {
        this.etatList.add(etat);
    }

    /**
     * permet de changer d'etat courant
     * le premier état qui correspond les conditions deviendra l'etat courant
     */
    public void changerEtat() {
        for (IEtat etat : this.etatList)
            if (etat.isTrigerred()) {
                this.etatActuel = etat;
                return;
            }
    }


    public void deplacement() {
        this.etatActuel.reaction();
    }


    /**
     *
     */
    public void solicitation() {
        // TODO implement here
    }

}