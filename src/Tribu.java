import Utils.Color;

import java.util.*;

/**
 *
 */
public class Tribu extends Membre {
    /**
     * Permet de générer des id pour chaque tribu
     */
    private static int TRIBUID;
    /**
     *
     */
    private Membre chef;
    /**
     *
     */
    private Set<Membre> membres;
    /**
     * permet de différencier chaque tribu
     * sert notament pour des equals ou pour générer la couleur
     */
    private int id;


    /**
     * Default constructor
     */
    public Tribu() {
        super();
        this.id = ++Tribu.TRIBUID;
        this.membres = new HashSet<>();
    }

    /**
     * créer une tribu enfant d'une tribu t
     *
     * @param t la tribu parente
     */
    public static Tribu tribuFromTribu(Tribu t) {
        Tribu trib = new Tribu();
        trib.rejoindreTribu(t);
        return trib;
    }

    /**
     * ajoute un membre à une tribu
     * s'il est le premier membre de la tribu, il devient directement le chef de celle-ci
     *
     * @param mb le membre à ajouter
     */
    public void ajouterMembre(Membre mb) {
        this.membres.add(mb);
        if (Objects.isNull(this.chef))
            this.mettreChef(mb);
    }

    /**
     * @return un boléen qui indique si la tribu possède une tribu mère ou non
     */
    public boolean isSub() {
        return super.possedeTribu();
    }

    public List<Tribu> getTribuMeres() {
        List<Tribu> res = new ArrayList<>();
        if (super.getTribu().isSub())
            this.getTribuMeres(res);
        return res;
    }

    private void getTribuMeres(List<Tribu> tribuList) {
        tribuList.add(this);
        if (super.getTribu().isSub())
            super.getTribu().getTribuMeres(tribuList);
    }

    public List<Membre> getMembres() {
        return (List<Membre>) this.membres;
    }

    public List<Race> getHabitants() {
        List<Membre> aVisiter = new ArrayList<>();
        List<Race> res = new ArrayList<>();
        aVisiter.addAll(this.membres);
        Membre nx;
        Iterator i = aVisiter.iterator();
        while (i.hasNext()) {
            nx = (Membre) i.next();
            if (nx instanceof Tribu)
                aVisiter.addAll(nx.getTribu().getMembres());
            else
                res.add((Race) nx);
        }
        return res;
    }


    /**
     * enlève la tribu mère associée à la tribu
     */
    public void independantiser() {
        super.quitterTribu();
    }

    /**
     * change de la tribu
     *
     * @param indivudu le nouveau chef
     */
    public void mettreChef(Membre indivudu) {
        this.chef = indivudu;
    }

    public Race getChef() {
        return (Race) this.chef;
    }

    /**
     * Les deux tribus demeurent avec des changements de membres
     *
     * @param tribuAutre la tribu avec laquelle le statuquo est effectué
     */
    public void statuquo(Tribu tribuAutre) {
        Random rd = new Random();
        for (Tribu t : new Tribu[]{this, tribuAutre})
            for (Membre mb : t.getHabitants())
                if (rd.nextFloat() > 0.8) {
                    mb.quitterTribu();
                    mb.rejoindreTribu(t.equals(tribuAutre) ? this : t);
                }
    }

    /**
     * Fusionne avec une autre tribu
     *
     * @param tribuPerdante la tribu qui a perdu lors de la négociation
     */
    public void fusion(Tribu tribuPerdante) {
        List<Race> toAdd = tribuPerdante.getHabitants();
        for (Membre mb : toAdd) {
            mb.quitterTribu();
            mb.rejoindreTribu(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tribu tribu = (Tribu) o;
        return Objects.equals(chef, tribu.chef) && Objects.equals(membres, tribu.membres);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chef, membres);
    }

    public Color getColor() {
        return Color.values()[this.id % Color.values().length];
    }

}