import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 */
public class Monde {
    /**
     *
     */
    private List<Parcelle> plateformes;
    private int tailleX;
    private int tailleY;

    public Monde(int tailleX, int tailleY) {
        this.tailleX = tailleX;
        this.tailleY = tailleY;
        this.plateformes = new ArrayList<Parcelle>(tailleX * tailleY);
        for (int x = 0; x < tailleX; ++x)
            for (int y = 0; y < tailleY; ++y)
                this.plateformes.add(new Parcelle());
    }

    public int getTailleX() {
        return this.tailleX;
    }

    public int getTailleY() {
        return this.tailleY;
    }

    /**
     * permet d'obtenir la parcelle à la position donnée
     *
     * @param posX absisse de la parcelle à trouver
     * @param posY ordonnée de la parcelle à trouver
     * @return la parcelle dont les coordonnées correspondent aux paramètres
     */
    public Parcelle getParcelle(int posX, int posY) {
        posX = posX < 0 ? this.tailleX - 1 : posX % this.tailleX;
        posY = posY < 0 ? this.tailleY - 1 : posY % this.tailleY;
        return this.plateformes.get(posY * this.tailleX + posX);
    }

    /**
     * Indique la position d'une parcelle dans le monde
     *
     * @param parc la parcelle dont on veut trouver les coordonnées
     * @return une liste contenant le couple (x,y) correspondant à la position de la parcelle dans le monde
     */
    public List<Integer> getPos(Parcelle parc) {
        List<Integer> pos = new ArrayList<>(2);
        pos.add(0, this.plateformes.indexOf(parc) % this.tailleY);
        pos.add(1, (this.plateformes.indexOf(parc) - pos.get(0)) / tailleY);
        return pos;
    }

    @Override
    public String toString() {
        String res = "";
        String separatorSpace = " ";
        for (int i = 1; i < this.plateformes.size() + 1; i++) {
            res += this.plateformes.get(i - 1);
            res += separatorSpace;
            if (i % this.tailleX == 0)
                res += "\n";
        }
        return res;
    }

    /**
     * Affichage du monde pour le tour en cours
     * @param individu
     * @return
     */
    public String toStringTour(Race individu){
        String res = "";
        String separatorSpace = " ";
        for (int i = 1; i < this.plateformes.size() + 1; i++) {
            res += this.plateformes.get(i - 1).toStringWithRace(individu);
            res += separatorSpace;
            if (i % this.tailleX == 0)
                res += "\n";
        }
        return res;

    }

    /**
     * déplace un individu à sa nouvelle position
     *
     * @param individu à déplacer
     * @param x        l'absisse de sa nouvelle position
     * @param y        l'ordonnée de sa nouvelle position
     */
    public Parcelle deplacer(Race individu, int x, int y) {
        x = x % this.tailleX;
        y = y % this.tailleY;
        Parcelle p = individu.getParcelle();
        if (Objects.nonNull(p))
            p.retirerRace(individu);
        Parcelle toAdd = this.getParcelle(x % this.tailleX, y % this.tailleY);
        toAdd.ajouterRace(individu);
        return toAdd;
    }

    public Parcelle[] getParcellesVoisines(Parcelle p) {
        List<Integer> pos = this.getPos(p);
        Parcelle[] pVoisines = new Parcelle[5];
        int x = pos.get(0);
        int y = pos.get(1);
        pVoisines[0] = this.getParcelle((x - 1) % this.tailleX, y);
        pVoisines[1] = this.getParcelle((x + 1) % this.tailleX, y);
        pVoisines[2] = this.getParcelle(x, (y - 1) % this.tailleY);
        pVoisines[3] = this.getParcelle(x, (y + 1) % this.tailleY);
        pVoisines[4] = this.getParcelle(x, y);
        return pVoisines;
    }

}