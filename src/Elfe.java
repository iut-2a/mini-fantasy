import Utils.Color;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 *
 */
public class Elfe extends Race {

    /**
     * tableau contenant toutes les actions réalisables par un elfe
     */
    private static Action[] listActions = new Action[]{
            Action.Deplacer, Action.Attendre, Action.CreerTribu
    };
    private static Action[] listActionChef = new Action[]{Action.Soliciter, Action.Negocier, Action.Emanciper};

    /**
     * Default constructor
     */
    public Elfe(Monde monde) {
        super(monde);
    }


    /**
     * fonde une tribu et en devient de chef
     */
    public void creerTribu() {
        Tribu t = new Tribu();
        this.rejoindreTribu(t);
    }

    @Override
    public void recruter(Race individu) {

    }

    @Override
    public void reagir() {
        super.reagir(); // todo
    }

    @Override
    public String getPrintValue() {
        String res = "";
        if (Objects.nonNull(this.getTribu()))
            res += this.getTribu().getColor();
        return res + "e" + Color.RESET;
    }


    @Override
    public Action[] getActionDisponibles() {
        if (!this.estChef())
            return Elfe.listActions;
        return Stream.concat(Arrays.stream(Elfe.listActionChef), Arrays.stream(Elfe.listActions)).toArray(Action[]::new);
    }


    /**
     * @return un boléen qui indique si l'elf est un chef de tribu ou non
     */
    public boolean estChef() {
        if (Objects.isNull(this.getTribu()))
            return false;
        return this.getTribu().getChef().equals(this);
    }


    @Override
    public char getDirectionDeplacementIA() {
        return 'o';
    }

    @Override
    public List<Action> getActionList() {
        return Arrays.asList(this.getActionDisponibles());
    }
}