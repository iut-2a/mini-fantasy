/**
 *
 */
public interface IEtat {


    /**
     * déclenche l'évènement dont s'occupe l'état
     */
    public void reaction();

    /**
     * @return un boléen qui indique si les conditions pour déclencher cet etat sont satisfaites
     */
    public boolean isTrigerred();

}