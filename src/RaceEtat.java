import java.util.List;

/**
 * Race mais qui peut posséder des états
 * il ne peut en avoir qu'un seul à la fois cependant
 */
public abstract class RaceEtat extends Race {

    /**
     *
     */
    protected AutomateEtat changeEtat;


    /**
     * Default constructor
     */
    public RaceEtat(Monde monde) {
        super(monde);
        this.changeEtat = new AutomateEtat(this);
    }

    /**
     * Ajouter un état déclenchable à l'individu
     * Un nouvel état est par défaut ajouté à la priorité la moins élevé
     *
     * @param etat
     */
    public void ajouterEtat(IEtat etat) {
        this.changeEtat.ajouterEtat(etat);
    }

    /**
     * Fait se rapprocher du chef du tribu
     */
    abstract void refugier();

    /**
     * fait fuir d'un chef de tribu ennemi
     * @param chefEtranger
     */
    abstract void fuir(Race chefEtranger);

    /**
     * fait chenger de tribu
     * @param tribu fiat quitter sa tribu à un Gnome et le fait rejoindre une autre
     */
    abstract void deserter(Tribu tribu);
}