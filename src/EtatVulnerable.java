import java.util.Objects;

/**
 *
 */
public class EtatVulnerable implements IEtat {

    private RaceEtat individu;

    /**
     * Default constructor
     */
    public EtatVulnerable(RaceEtat individu) {
        this.individu = individu;
    }

    /**
     *
     */
    public void reaction() {
        this.individu.refugier();
    }

    /**
     * L'individu est toujours isolé à défaut que l'individu ait un autre état
     *
     * @return un boléen true
     */
    @Override
    public boolean isTrigerred() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EtatVulnerable that = (EtatVulnerable) o;
        return Objects.equals(individu, that.individu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(individu);
    }
}