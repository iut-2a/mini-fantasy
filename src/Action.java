import java.util.Objects;
import java.util.Random;

/**
 * Liste de toutes les actions possibles entre 0..n individus
 */
public enum Action {

    /**
     * Demande à tous les gnomes sur sa parcelle et les voisins de rejoindre sa tribu
     * Un seul paramètre est nécessaire
     */
    Soliciter("soliciter") {
        @SafeVarargs
        @Override
        public final void execute(Race... individus) {
            Race individu = individus[0];
            for (Parcelle p : individu.getMonde().getParcellesVoisines(individu.getParcelle()))
                for (Race indiv : p.getMatching(Gnome.class))
                    if (Objects.isNull(indiv.getTribu()))
                        Action.Recruter.execute(individu, indiv);
        }
    },
    /**
     * Envoie une demande pour rejoindre la tribu
     * 2 paramètres sont nécessaires, le recruteur et le recruté
     */
    Recruter("recruter") {
        @SafeVarargs
        @Override
        final void execute(Race... individus) {
            Race individu = individus[0];
            Race ind2 = individus[1];
            ind2.addEvent(new Recrutement(individu, ind2));
            ind2.getEvents().get(0).consume();
        }
    },

    /**
     * Déplace l'individu dans la direction souhaité
     */
    Deplacer("deplacer") {
        @SafeVarargs
        @Override
        final void execute(Race... individus) {
            char dir = individus[0].getDirectionDeplacement();
            individus[0].seDeplacer(dir);
        }
    },
    Emanciper("emanciper") {
        @SafeVarargs
        @Override
        final void execute(Race... individus) {
            individus[0].quitterTribu();
        }
    },
    /**
     * Permet de passer son tour
     */
    Attendre("attendre") {
        @SafeVarargs
        @Override
        final void execute(Race... individus) {
            return;
        }
    },
    /**
     * Envoie une de négociation à un autre chef de tribu
     * * 2 paramètres sont nécessaires, le premeir chef et le deuxième chef
     */
    Negocier("negocier") {
        @SafeVarargs
        @Override
        final void execute(Race... individus) {
            Race chef1 = individus[1];
            Race chef2 = individus[0];
            if (chef1.getTribu().getTribuMeres().contains(chef2.getTribu())) {
                System.out.println("Erreur le chef fait parti d'une des tribus dominatrices");
                return;
            }
            if (chef2.getTribu().getTribuMeres().contains(chef1.getTribu())) {
                System.out.println("Erreur l'elf fait parti d'une des tribus dominatrices du chef");
                return;
            }
            Random rd = new Random();
            boolean aGagne = rd.nextBoolean();
            if (aGagne)
                chef2.getTribu().statuquo(chef1.getTribu());
            else
                chef2.getTribu().fusion(chef1.getTribu());
        }
    },
    /**
     * Action avec 1 seul elfe
     * Il fonde sa tribu et la rejoint
     */
    CreerTribu("CreerTribu") {
        @Override
        void execute(Race... individus) {
            Tribu tribu = new Tribu();
            individus[0].rejoindreTribu(tribu);
            tribu.ajouterMembre(individus[0]);
        }
    };

    /**
     * Nom de l'action à effectuer
     */
    private final String label;

    Action(String label) {
        this.label = label;
    }

    /**
     * exécute l'action en question
     *
     * @param individus toutes les personnes concernées par l'action
     */
    abstract void execute(Race... individus);

}


